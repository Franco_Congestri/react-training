import React from 'react';
import {List} from 'antd';

function ProductList(props) {
    return (
      <>
        <List
          header={<div>Header</div>}
          footer={<div>Footer</div>}
          bordered
          dataSource={props.list}
          renderItem={item => (
            <List.Item>
                {item}
            </List.Item>
          )}
        />
      </>  
    )
}
export default ProductList;