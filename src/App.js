//import logo from './logo.svg';
import { Content, Footer } from 'antd/lib/layout/layout';
import { useState } from 'react';
import './App.css';
import ProductList from './components/ProductList'; 
import Inputs from './components/Inputs';

function App() {
  const [data, setData] = useState([]);
  return (
    <div className="App">
      <header className="App-header">
        <h1 style={{color:'white'}}>
          Este es un titulo
        </h1>
      </header>
      <Content className="App-content">
        <Inputs setData = {setData} data={data}/>
        <ProductList list={data}/>
      </Content>
      <Footer className="App-footer">
       {/* <img src={logo} className="App-logo" alt="logo" />  */}
          <p>
            Este es un footer
          </p>
          <a
           className="App-link"
            href="https://reactjs.org"
           target="_blank"
           rel="noopener noreferrer"
          >
            Learn React
          </a> 
      </Footer>
    </div>
  );
}

export default App;
